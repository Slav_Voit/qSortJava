package qsort;

public class Main {
    static void quickSort(int arr[], int left, int right) {


        int i = left, j = right;
        int pivot = arr[(left + right) / 2];

        while (i <= j) {
            while (arr[i] < pivot)
                i++;
            while (arr[j] > pivot)
                j--;
            if (i <= j) {
                int tmp;
                tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
                i++;
                j--;
            }
        }
        int index = i;
        if (left < index - 1)
            quickSort(arr, left, index - 1);
        if (index < right)
            quickSort(arr, index, right);
    }

    public static void main(String[] args) {
        int k = 30;
        int mas[] = new int[k];
        for (int i = 0; i < k; i++) {
            mas[i] = (int) (Math.random() * 10);
            System.out.print(mas[i] + " ");
        }
        quickSort(mas, 0, k - 1);
        System.out.println();
        for (int i = 0; i < k; i++) {
            System.out.print(mas[i] + " ");
        }
    }
}
